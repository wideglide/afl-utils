# DESCRIPTION:  Install AFL-Utils
# COMMENTS:
#   This builds a container with all the AFL tools
#   necessary to get started fuzzing. This is intended
#   to be used as a base container that specific fuzzing
#   targets/sessions build from
#
# USAGE:
#	# Clone project
#     git https://gitlab.com/wideglide/afl-utils
#
#	# Build image
#	  docker build -t afl-utils .
#
#   # Run container (will put you in a shell with all tools available)
#	  docker run --rm -it afl-utils
#

FROM registry.gitlab.com/wideglide/afl/i386:16.04

# Install dependencies
USER root
RUN apt-get update && apt-get -qq install -y --no-install-recommends \
      libpython3-dev \
      python3-setuptools \
      htop \
      tmux \
    && apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install afl-utils
COPY . /afl-utils
RUN cd /afl-utils && \
    chmod 755 /set_umask.sh && chown fuzz /set_umask.sh && \
    easy_install3 . && \
    mkdir -p /home/fuzz && \
    echo "source /usr/local/lib/python3.5/dist-packages/exploitable-1.33-py3.5.egg/exploitable/exploitable.py" >> /root/.gdbinit && \
    echo "source /usr/local/lib/python3.5/dist-packages/exploitable-1.33-py3.5.egg/exploitable/exploitable.py" >> /home/fuzz/.gdbinit

USER fuzz

ENTRYPOINT ["./set_umask.sh"]
CMD /bin/bash
